import React from "react"
import "./style.scss"
import classNames from "classnames"

const Container = ({ className, children }) => {
    return (
        <div className={classNames('container-custom', className)}>
            {children}
        </div>
    )
}
export default Container;