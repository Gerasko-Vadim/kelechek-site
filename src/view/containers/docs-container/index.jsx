import React from "react"
import { files } from "../../../files";
import { DocItem } from "../../components/doc-item";
import "./style.scss"

const DocsContainer = ({ items }) => {
    return (
        <div className="doc-wrapper">
           
            {
                files.map((item) => <DocItem pathname={item.path} name={item.name} />)
            }

        </div>
    )
}
export default DocsContainer;