import React, { useEffect } from "react"
import { useLocation } from "react-router"
import renderHTML from 'react-render-html';
import "./style.scss"
import { oferta } from "../docs/files/oferta";
import { files } from "../../../files";
import { UserInfo } from "../docs/files/info-users";


const Doc = () => {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    const { pathname } = useLocation()
    console.log(pathname)
    const file = files.find((file) => file.path === pathname.split('/').pop())
    return (
        <div className="doc-page">
            <div className="doc-page__content">
                {renderHTML(file.file)}
         
            </div>
        </div>
    )
}
export default Doc;