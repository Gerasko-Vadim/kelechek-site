export const loanAgreement = `
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Noto Sans Symbols";}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{    margin-top: 0in;
    margin-right: 0in;
    margin-bottom: 8.0pt;
    margin-left: 0in;
    line-height: 107%;
    font-size: 11.0pt;}
.MsoChpDefault
	{font-size:12.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:28.35pt 42.5pt 26.95pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><b><span style='font-family: "Times New Roman"' lang=RU>ПУБЛИЧНЫЙ
КРЕДИТНЫЙ ДОГОВОР</span></b></p>

<p class=MsoNormal style='text-align: justify;'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>Настоящий Публичный
Кредитный Договор ОсОО «МКК «Келечек» (далее - МКК) действующего на основании
Свидетельства об учетной регистрации от 6 августа 2021 года за № 510, выданной
Национальным банком Кыргызской Республики, для физических лиц в рамках
потребительского кредитования, действует в рамках Публичной оферты ОсОО «МКК
«Келечек» О заключении договора присоединения к сервису «Cash2U» (Далее –
Оферта), опубликованных на Интернет - ресурсе МКК по адресу: www.cahs2u.kg и в
разделе «Документы» Мобильного приложения «Cash2U» (далее - МП), и определяет
условия Кредитного договора, заключаемого МКК с физическими лицами (далее -
Заемщик) в рамках потребительского кредитования.</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ПРЕДМЕТ И УСЛОВИЯ КРЕДИТОВАНИЯ </span> </b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
По настоящему договору МКК, в случае одобрения
уполномоченным органом МКК, предоставляет Заемщику в кредит денежные средства
(далее – «кредит») на приобретение товаров/услуг (далее - товар) у торговой
организации, с которой МКК заключено соглашение о сотрудничестве (далее -
торговая организация), а Заемщик обязуется возвратить кредит на условиях,
определенных настоящим договором.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Настоящий Договор между сторонами заключается путем
акцепта заемщиком Оферты. Акцептом оферты является совершение заемщиком
действий в соответствии с условиями Оферты, которые рассматриваются как полное
и безусловное согласие с условиями Оферты и настоящего Договора. Акцептом
условий Оферты Заемщик безоговорочно соглашается с условиями публичного
кредитного договора о получении кредита через сервис «Cash2U».</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Предоставление кредита осуществляется путем
зачисления денег в пользу торговой организации в счет оплаты Товара по заявке
Заемщика, направляемой посредством МП в рамках предоставленного Заемщику лимита
Кредита. Оплата товаров, приобретаемых Заемщиком, осуществляется на условиях,
определяемых МКК и торговой организацией. Если иное не предусмотрено Договором,
кредит считается предоставленным с момента оплаты Товара.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Погашение Кредита осуществляется Заемщиком
ежемесячно, безналичным способом по реквизитам, указанным в МП, равными
платежами в течение 3 (трех) месяцев, начиная со следующего месяца после
получения Кредита, при этом Заемщик вправе выбрать число месяца из следующих:
5, 10, 15, 20. Первый взнос погашения кредита производится не позднее
указанного Заемщиком числа месяца, следующего после получения Кредита, и в
дальнейшем взносы на погашение Кредита должны производится в указанное число
соответствующего месяца.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
График погашения Кредита формируется на каждый
транш выданного Кредита и доступен для просмотра в разделе «Графики погашений»
МП.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'  lang=RU>1.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Сумма кредита, предоставляемая МКК Заемщику,
составляет от 5&nbsp;000 до 15&nbsp;000 сом и устанавливается МКК по своему
усмотрению в зависимости от информации, предоставленной Заемщиком. Кредит
предоставляется в виде возобновляемой кредитной линии, после погашения
Заемщиком задолженности по предыдущим заявкам, сумма погашения доступна для
повторного использования Заемщиком в рамках кредитного лимита, Заемщик вправе
использовать Кредит в рамках установленного лимита по заявке, направляемой
посредством МП. Кредитный лимит предоставляется Заемщику сроком 24 (двадцать
четыре) месяца с момента утверждения кредитного лимита.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Процентная ставка по Кредиту устанавливается в
размере 0% (ноль процентов) годовых, эффективная процентная ставка составляет
0% (ноль процентов) годовых.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В случае неиспользования Заемщиком кредита в
течение 30 (тридцати) последовательных дней лимит предоставленный Заемщику по
его заявке аннулируется и становится недоступным для использования. В этом
случае Заемщик вправе обратиться в МКК посредством МП с повторной заявкой,
которая рассматривается в порядке, установленном настоящим Договором.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Срок действия настоящего Договора<b> </b>– до
полного исполнения Сторонами обязательств по Договору.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.10.&nbsp;&nbsp;
МКК предоставляет Заемщику всю необходимую
информацию путем размещения ее в МП.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.11.&nbsp;&nbsp;
Применение электронной подписи является
эквивалентом собственноручной подписи Заемщика, инструментом идентификации
Заемщика и проверки целостности электронного документа.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.12.&nbsp;&nbsp;
Подписание оферты и Договора электронной подписью
не является гарантией принятия положительного решения МКК на предоставление
Кредита.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.13.&nbsp;&nbsp;Заемщик осведомлен и понимает юридическую
значимость операций и сделок, совершаемых с использованием электронной подписи.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.14.&nbsp;&nbsp;
Заемщик понимает, что подписанный электронной
подписью настоящий документ (пакет документов) является безотзывным и
признается равнозначным документу (пакету документов) на бумажном носителе.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.15.&nbsp;&nbsp;
За просрочку возврата и/или не возврат Заемщиком
основной суммы кредита в сроки, установленные настоящим Договором и
соответствующими заявками Заемщика, начисляется пеня в размере 1 (один) % от
просроченной суммы основного долга за каждый день просрочки. При этом размер
пени, начисленной за весь период действия кредита не должен превышать 20 %
(Двадцать процентов) от суммы выданного кредита.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.16.&nbsp;&nbsp;
Целевое назначение кредита – приобретение Товаров в
торговой организации.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.17.&nbsp;&nbsp;
Комиссионные сборы, оплачиваемые Заемщиком при
предоставлении Кредита не предусмотрены. Иные комиссионные сборы указаны в
Паспорте кредитного продукта «Онлайн» в разделе «Документы» МП.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>1.18.&nbsp;&nbsp;
Заемщик вправе досрочно без каких-либо штрафных
санкций погасить задолженность по кредиту в полном объеме либо частично. </span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>2.ПОРЯДОК ПРЕДОСТАВЛЕНИЯ И ПОГАШЕНИЯ КРЕДИТА </span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><a name="_heading=h.gjdgxs"></a><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; МКК производит оплату Товара торговой организации по заявке Заемщика на
условиях настоящего Договора, Оферты и договоров, заключаемых МКК с торговой
организацией.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МКК предоставляет Заемщику кредит в национальной валюте
Кыргызской Республики - сомах.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МКК вправе письменно уведомить и отказаться от
предоставления Заемщику предусмотренного настоящим договором кредита полностью
или частично при наличии следующих обстоятельств: </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
если будут установлены обстоятельства, очевидно
свидетельствующие о том, что сумма Кредита не будет возвращена в срок; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
если будет установлено, что Заемщик предоставил МКК
заведомо недостоверные сведения о своем финансовом состоянии, или иной попытки
злоупотребления доверием МКК. </span></p>

<p class=MsoNormal style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>МКК самостоятельно
определяет факт наличия или отсутствия указанных обстоятельств и не обязан
предоставлять Заемщику доказательства их наличия.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Начисление МКК процентов за просрочку возврата
суммы основного долга начинается со дня возникновения задолженности по
основному долгу кредита и рассчитывается исходя из фактического количества дней
просрочки по основному долгу и количество дней в году равного 360, производится
на сумму просроченной задолженности по основному долгу в течение каждого
календарного месяца по количеству дней в месяце вплоть до полного погашения
задолженности по основному долгу.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщик производит возврат кредита, уплату
неустойки, сборов и платежей в той же валюте, в которой получен кредит. </span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Если Заемщик не полностью внесет сумму, подлежащую погашению,
то распределение внесенных денежных средств осуществляется МКК в соответствии с
очередностью платежей по кредиту, установленной действующим законодательством
Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МКК вправе приостановить дальнейшую выдачу кредита
и/или потребовать его досрочного погашения в следующих случаях: </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщик оказался не в состоянии оплатить любую
сумму, подлежащую оплате в соответствии с условиями настоящего Договора; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщик допустил любое другое нарушение своих
обязательств по настоящему Договору; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
сведения, справки, документы и т.п.,
предоставленные Заемщиком МКК в соответствии с условиями настоящего Договора,
оказались недостоверными; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщик или зависимые от него лица признаны
неплатежеспособными, привлечены к уголовной или административной ответственности
за нарушения законодательства с конфискацией имущества (доходов), размер
которого МКК признает существенным; </span></p>

<p class=MsoNormal style='margin-left:.5in;text-align:justify;text-indent:-.25in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщику (или зависимым от него лицам) будет
предъявлен иск об уплате денежной суммы или об истребовании имущества, размер которого
МКК признает значительным.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В случаях, указанных в пункте 2.7. настоящего
Договора, МКК направляет Заемщику уведомление о прекращении дальнейшего
кредитования и/или о досрочном возврате суммы кредита.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщик обязуется производить погашение кредита,
неустойки, сборов и платежей в безналичной форме путем перечисления методами,
указанными в МП. </span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>2.10.&nbsp;&nbsp;
Стороны настоящим Договором определили, что при
ненадлежащем исполнении и/или неисполнении Заемщиком своих обязательств по
настоящему Договору, МКК вправе предоставить любую информацию о Заемщике
третьим лицам любым удобным ему способом, включая размещение информации в
средствах массовой информации, с целью обеспечения возврата кредита.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ОБЯЗАТЕЛЬСТВА И ПРАВА СТОРОН</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU>3.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span style='font-family: "Times New Roman"' lang=RU>Заемщик по настоящему Договору обязуется:</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Производить возврат основной суммы кредита согласно
Графику платежей;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Незамедлительно уведомить МКК об изменении адреса,
контактных данных (сотовая связь, телефон, электронная почта) реквизитов и обо
всех других обстоятельствах, связанных с надлежащим исполнением своих
обязательств по настоящему Договору, а также принять все меры к устранению
любых причин, могущих повлечь ненадлежащее исполнение и/или неисполнение
условий и обязательств по настоящему Договору;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
По первому требованию МКК произвести
незамедлительное погашение кредита в случаях, предусмотренных настоящим
Договором или законодательством Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В случае возврата товара обратиться в МКК для
погашения остатка задолженности в случае, если перечисленная торговой организацией
сумма не покрывает остаток задолженности по займу.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В случае неисполнения или ненадлежащего исполнения
настоящего Договора, оплатить все издержки МКК, включая транспортные расходы и
расходы на внешних консультантов, понесенные МКК в связи с взысканием
задолженности, а также расходы, связанные с подготовкой, изучением и
проведением переговоров, регистрацией, нотариальным заверением,
администрированием, внесением поправок, прекращением документов, отменой и
принудительным исполнением условий настоящего Договора и других договоров,
имеющих отношение к настоящему Договору, и сохранением прав МКК.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Исполнять иные обязанности, предусмотренные Офертой
и документами, входящими в состав Оферты.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.1.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Самостоятельно отслеживать изменения, внесенные МКК
в Оферту и (или) тарифы.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU>3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span style='font-family: "Times New Roman"' lang=RU>Заемщик имеет право</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В рамках кредитования возвратить товар в
соответствии с требованиями законодательства.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Досрочно погасить Кредит частично или в полном
объеме в любое время без оплаты неустойки или иных видов штрафных санкций.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Отказаться на безвозмездной основе от получения
кредита с момента подписания Публичного кредитного договора до осуществления
платежа в оплату за Товар по договору.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Иметь доступ к Публичному кредитному договору со
всеми прилагаемыми к нему документами и обратиться за юридической консультацией
за пределами МКК, без ограничений по времени.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Получить разъяснения по порядку расчетов платежей
по кредиту, пени, штрафных санкций.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.2.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Выбрать язык (государственный или официальный), на
котором будет составлен (оформлен) кредитный договор.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МКК по настоящему Договору обязуется:</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.3.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В течение трех рабочих дней по запросу Заемщика
предоставить информацию о получении кредита Заемщиком в МКК и о соблюдении
кредитной дисциплины Заемщика по данному кредиту для ее предоставления в другое
финансово-кредитное учреждение;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.3.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Прекратить начисление неустойки (штрафов пени) по
истечению 15 дней с момента направления Извещения о начале процедура обращения
взыскания по кредиту Заемщика.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU>3.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span style='font-family: "Times New Roman"' lang=RU>МКК имеет право</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Отказать Заемщику в предоставлении Кредита и не
давать информацию относительно принятого решения;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В одностороннем порядке вносить изменения и
дополнения в Оферту и (или) тарифы. Уведомление Заемщика об изменении оферты и
(или) тарифов осуществляется МКК не позднее чем за 10 (десять) дней до даты
введения в действие изменений/новой редакции Оферты и (или) тарифов путем
размещения текста изменений/новой редакции Оферты и (или) тарифов на сайте МКК
и в МП. Любые изменения Оферты и (или) тарифов становятся обязательными для сторон
с даты введения их в действие;</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Требовать досрочного возврата суммы кредита при
нарушении Заемщиком срока, установленного для возврата очередной части кредита,
если иное не предусмотрено требованиями законодательства Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Передавать информацию о Заемщике, полученную в ходе
заключения и исполнения Договора, третьим лицам, в том числе в случае уступки
МКК своих прав по Договору третьим лицам.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Уступать требования по Договору третьим лицам без
согласия Заемщика.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Записывать телефонные разговоры с Заемщиком и
использовать звукозаписи/видеозаписи в качестве доказательств при рассмотрении
споров между МКК и Заемщиком, а также в иных случаях.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Направлять Заемщику информацию о деятельности,
продуктах и услугах МКК.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Совершать иные действия, предусмотренные
законодательством и Договором.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>3.4.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Передать задолженность на досудебные взыскание и
урегулирование коллекторскому агентству или иной специализированной организации
по своему усмотрению при допущении Заемщиком просрочки исполнения обязательств
по Договору.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ФОРС-МАЖОР</span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>4.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Стороны освобождаются от ответственности за
частичное или полное неисполнение обязательств по настоящему Договору, в случае
возникновения обстоятельств непреодолимой силы (форс-мажора), к которым
относятся: стихийные бедствия, пожары, наводнения, военные действия, вступление
в силу законодательных актов, актов органов власти и управления, обязательных
для исполнения одной из сторон, прямо или косвенно запрещающих указанные в
настоящем Договоре виды деятельности, или вызванные иными обстоятельствами вне
разумного контроля Сторон, препятствующие выполнению Сторонами своих
обязательств по настоящему Договору.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>4.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Сторона, у которой возникли Форс-мажорные
обстоятельства, обязана уведомить о наступлении таких обстоятельств, в течение
трех дней с момента наступления Форс-мажорных обстоятельств. Форс-мажорные
обстоятельства должны подтверждаться документами уполномоченных государственных
органов.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>4.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В случаях наступления обстоятельств Форс-мажора
срок исполнения обязательств по настоящему Договору отодвигается соразмерно
времени, в течение которого действуют такие обстоятельства и их последствия. </span></p>

<p class=MsoNormal style='text-align:justify;border:none'><a
name="_heading=h.30j0zll"></a><span lang=RU>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in;border:none'><b><span style='font-family: "Times New Roman"'
lang=RU>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ПРОЧИЕ ПОЛОЖЕНИЯ </span></b></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Досрочное расторжение настоящего Договора может
иметь место по соглашению сторон, а также в случаях, предусмотренных настоящим
Договором или по иным основаниям, предусмотренным гражданским законодательством
Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Если какое-либо положение настоящего Договора
становится недействительным, незаконным или неприменимым в каком-либо отношении
и по какому-либо закону, действительность, законность или применимость
остальных положений не должна быть каким-либо образом снижена или отменена. В
таких случаях, при необходимости, МКК в соответствии с законодательством
Кыргызской Республики вносит изменения в настоящий Договор для замены положения,
признанного недействительным, незаконным, иным положением, действительным,
законным и идентичным по содержанию и назначению настоящему Договору,
соответствующим интересам Сторон и законодательству Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Все споры или разногласия, возникающие между
Сторонами по настоящему Договору или в связи с ним, разрешаются путем
переговоров между Сторонами. Стороны настоящим Договором определили, что в
случае невозможности разрешения разногласий путем переговоров, любые споры,
возникающие и/или связанные с настоящим Договором, в том числе споры касающиеся
заключения, нарушения, прекращения, расторжения или недействительности
настоящего Договора, подлежат разрешению в судах в порядке, установленном
законодательством Кыргызской Республики, по месту нахождения МКК.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В случае не уведомления Заемщиком об изменении
адреса жительства, контактной информации, уведомления, направленные МКК
заказной почтой по адресу, указанному Заемщиком, или по иной контактной
информации, считаются полученными Заемщиком.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Во всем, что не предусмотрено
настоящим Договором, Стороны руководствуются законодательством Кыргызской
Республики.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
В случае если настоящим
Договором установлены иные положения, отличающиеся от положений Оферты,
приоритет имеют положения настоящего Договора, соответственно, положения Оферты
в этой части не применяются к Заемщику или изменяются в зависимости от
содержания условий, изложенных в настоящем Договоре</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщик принимает условия
Оферты в целом и подтверждает, что:</span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Все документы входящие в состав Оферты прочитаны,
приняты Заемщиком в полном объеме, без каких- либо замечаний и возражений, не
содержат каких-либо обременительных для Заемщика условий, которые, исходя из
разумно понимаемых интересов Заемщика, не были бы приняты;</span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
настоящий Договор в совокупности с Офертой являются
единым документом;</span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU style='font-family:"Noto Sans Symbols"'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Заемщик не вправе ссылаться на отсутствие его
подписи на Оферте и в настоящем Договоре, как доказательство того, что Оферта и
настоящий Договор не были им прочитаны/поняты/приняты, если документы подписаны
простой электронной подписью согласно условиям Оферты;</span></p>

<p class=MsoNormal style='margin-left:49.65pt;text-align:justify;text-indent:
-21.3pt;background:white;border:none'><span lang=RU style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;'>−&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
заключение Договора и
исполнение его условий, в том числе, предоставление Кредита по Договору не
нарушит и не приведет к нарушению любой нормы законодательства Кыргызской
Республики.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
МКК не несет ответственность за
любые убытки, возникшие у Заемщика, в том числе в связи с тем, что Заемщик не
ознакомился и (или) несвоевременно ознакомился с условиями Оферты и (или)
тарифами и (или) изменениями и дополнениями, внесенными в Оферту и (или)
тарифы.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
С Офертой и документами
входящими в состав Оферты Заемщик может ознакомиться на сайте МКК по следующей
ссылке: www.cahs2u.kg и в МП.</span></p>

<p class=MsoNormal style='margin-left:0in;text-align:justify;text-indent:0in;
background:white;border:none'><span style='font-size: 12pt; line-height: 107%; font-family: "Times New Roman", serif; color: black;' lang=RU>5.10.&nbsp;&nbsp;
По всем имеющимся вопросам
Заемщик может обратиться в круглосуточный Контакт-центр по телефону: 0552 55 33
33, 0502 55 33 33,  0772 55 33 33 или обратиться в МКК.</span></p>

<p class=MsoNormal style='text-align:justify;background:white;border:none'><span
lang=RU>&nbsp;</span></p>

</div>

</body>

</html>

`