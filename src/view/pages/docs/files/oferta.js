export const oferta = `
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Noto Sans Symbols";}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:56.7pt 42.5pt 56.7pt 85.05pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>ПУБЛИЧНАЯ ОФЕРТА</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>О
заключении договора присоединения к сервису «Cash2U»</span></b></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>Настоящая оферта
является официальным предложением ОсОО «МКК «Келечек» физическим лицам о
присоединении к сервису «Cash2U» посредством мобильного приложения в
соответствии с пунктом 2 статьи 398 Гражданского кодекса Кыргызской Республики.
</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Предмет Договора</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>ОсОО «МКК «Келечек» (далее
«МКК») действующее на основании Свидетельства об учетной регистрации от 6
августа 2021 года за № 510, выданной Национальным банком Кыргызской Республики,
настоящим направляет публичное предложение предоставления услуг МКК, включая
онлайн кредитование, физическим лицам (далее «Клиент») на условиях,
предусмотренных в настоящей публичной оферте (далее «Оферта») и иных договорах,
заключаемых путем их подписания простой электронной подписью посредством
использования Мобильного приложения (далее «МП»).</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящий договор между
сторонами заключается путем акцепта клиентом настоящей Оферты. Акцептом Оферты
является совершение клиентом действий по прохождению идентификации и
регистрации в мобильном приложении, которые рассматриваются как полное и
безусловное согласие с условиями Оферты. </span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МКК вправе в одностороннем
порядке вносить изменения и дополнения в Оферту с уведомлением Клиента путем их
публикации в соответствующем разделе мобильного приложения и на сайте МКК по
адресу www.cash2u.kg за 10 дней до вступления их в силу. С момента вступления в
силу изменений и дополнений в Оферту они становятся обязательными для Сторон,
при этом в случае если Клиент не согласен с такими изменениями и дополнениями
он обязуется прекратить использование мобильного приложения.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Клиент, принимая условия
настоящей Оферты, гарантирует, что является собственником мобильного
устройства, на котором установлено мобильное приложение, и владельцем
абонентского номера сотового оператора с которого производится регистрация в
мобильном приложении.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>При регистрации Клиента в
мобильном приложении Клиент подтверждает, что ознакомлен со всеми положениями
документов входящих в состав настоящей Оферты, а именно:</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Публичная оферта;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif'>Публичный к<span style='color:black'>редитный
договор;</span></span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Согласие на обработку данных;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.5in;text-align:justify;text-indent:-.25in;line-height:normal;
border:none'><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'>-<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;font-family:"Times New Roman",serif;
color:black'>Иные документы находящиеся в разделе «Документы» Мобильного
приложения и на вебсайте МКК,</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif'>безусловно и в
полном объеме, без изъятий, принимает все обязательства и обязуется соблюдать
требование, содержащиеся в указанных документах.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>1.6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Использование клиентом МП
означает согласие с настоящей офертой и условиями обработки персональных данных
клиента, указанных в «Согласие на обработку данных» размещенных в МП в разделе
«Документы».</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Способы предоставления Услуги</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Услуги МКК предоставляются
посредством использования мобильного приложения «Cash2U» устанавливаемого на
мобильный телефон Клиента.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Клиент после установки
мобильного приложения «Cash2U» обязан указать все запрашиваемые данные,
заполнить предлагаемые формы и пройти </span><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif;color:#3C4043;
background:white'>идентификацию </span><span lang=RU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman",serif;color:black'>(установление
личности) согласно правилам и требованиям, установленным МКК в мобильном
приложении, а также предоставить согласие на обработку его персональных данных.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Условия предоставления услуг
указываются в Паспорте услуги размещенном в разделе «Документы» МП. Запрос
Клиента на использование услуги посредством МП означает, что Клиент ознакомлен
со всеми документами, согласен со всеми условиями и подписал соответствующие
документы, включая договора, формы и анкеты, электронной цифровой подписью,
генерируемой при регистрации/</span><span lang=RU style='font-size:12.0pt;
line-height:107%;font-family:"Times New Roman",serif;background:white'>аутентификации</span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'> Клиента в МП.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МКК рассматривает электронное
заявление Клиента и в случае его одобрения, согласно внутренним положениям МКК,
предоставляет Клиенту запрашиваемую услугу. При этом МКК вправе отказать
Клиенту в запрашиваемой услуге в случае несоответствия предоставленной Клиентом
информации нормам и требованиям внутренних документов МКК и/или
законодательства Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Все действия, совершаемые
Клиентом посредством МП «Cash2U» считаются юридически значимыми и
приравниваются к подписанным договорам/заявкам подаваемым Клиентом МКК.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>В случае утери мобильного
телефона, на который было установлено МП, утери </span><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
background:white'>аутентификационных </span><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif;color:black'>данных,
логина, пароля или иных существенных сведений, Клиент обязуется незамедлительно
информировать МКК об указанных обстоятельствах для блокировки аккаунта. Клиент
вправе восстановить доступ к услугам путем предоставления соответствующего
заявления МКК и </span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif'>создания <span style='color:black'>новых
логина и/или пароля.</span></span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Клиент вправе ознакомиться с
текущими услугами и со всей необходимой информацией по услугам в личном
кабинете МП.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>2.8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Все заявления, направляемые
посредством МП, считаются подтвержденными и окончательными (безусловными и
безотзывными) с момента их направления МКК посредством МП. Для Клиента
Заявление считается безотзывным и окончательным в момент его отправления МКК и
получения подтверждения о принятии Заявления к исполнению.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Права и обязанности Сторон</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Клиент имеет право</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Заключать с МКК договора
способами, предусмотренными настоящей Офертой, подтвердив данными действиями,
что Клиент не ограничен в дееспособности, не состоит под опекой,
попечительством, а также патронажем, по состоянию здоровья может самостоятельно
осуществлять и защищать свои права и исполнять обязанности, не страдает
заболеваниями, препятствующими осознавать суть заключаемых договоров и
обстоятельств его заключения.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Совершать любые
предусмотренные Офертой и не запрещенные законодательством Кыргызской
Республики операции.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Клиент обязан</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Своевременно и в полном объеме
ознакамливаться и соблюдать условия оферты, договоров и иных документов МКК,
размещенных в разделе «Документы» МП.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Предоставить информацию о
персональных данных, необходимую для пользования услугами МКК, обновлять,
дополнять предоставленную информацию о персональных данных в случае их
изменения.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Самостоятельно отслеживать
изменения, внесенные МКК в настоящую Оферту и (или) иные документы.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Нести ответственность за все
операции, совершенные до момента получения МКК от Клиента уведомления об утрате
</span><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:#3C4043;background:white'>авторизационных </span><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>данных.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.2.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Предпринимать всевозможные
меры для предотвращения утраты Авторизационных данных, не допускать незаконного
использования Авторизационных данных третьими лицами, в том числе не сообщать
третьим лицам Авторизационные данные, за исключением случаев, предусмотренных
законодательством Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МКК имеет право</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.3.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Запрашивать у Клиента
информацию, необходимую для предоставления услуг, в том числе информацию
персонального характера.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.3.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Проверять предоставленную
Клиентом информацию, в том числе путем направления запросов в государственные
органы и учреждения.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.3.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Расширять, обновлять,
дополнять перечень предоставляемых услуг посредством МП с уведомлением Клиента
путем размещения соответствующей информации в МП и на вебсайте МКК.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><a name="_heading=h.1fob9te"></a><span lang=RU style='font-size:
12.0pt;line-height:107%;font-family:"Times New Roman",serif;color:black'>3.3.4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>Блокировать доступ Клиента к услугам в случае подозрения и/или
выявления противоправных действий со стороны Клиента и/или третьих лиц, а также
в случае сбоев, в работе программного обеспечения, связи, при проведении профилактических
работ и в иных случаях, установленных в локальных актах МКК и законодательстве
Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>3.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МКК обязано</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.4.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Обеспечить хранение
конфиденциальной информации в тайне, не разглашать без предварительного
письменного разрешения Клиента информацию, а также не осуществлять обмен,
опубликование, либо разглашение иными возможными способами переданных
персональных данных Клиента, за исключением случаев, установленных Офертой
и/или законодательством Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.4.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Принимать меры предосторожности
для защиты конфиденциальности персональных данных Клиента согласно порядка,
используемого для защиты такого рода информации в существующем деловом обороте
и требований законодательства Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>3.4.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Принимать необходимые
организационные и технические меры для защиты персональной информации
клиента/заемщика от неправомерного или случайного доступа, уничтожения,
изменения, блокирования, копирования, распространения, а также от иных
неправомерных действий третьих лиц.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ответственность сторон</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>За неисполнение или
ненадлежащее исполнение обязательств по Оферте стороны несут ответственность в
соответствии с законодательством Кыргызской Республики.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Клиент самостоятельно несет
ответственность за достоверность номера мобильного телефона/адреса электронной
почты, логина и пароля указанных в заявлении. В случае недостоверности
указанного номера мобильного телефона/адреса электронной почты, логина и пароля
а также в иных случаях, обусловленных причинами, не зависящими от МКК (сообщение
не отправлено оператором сотовой связи, интернет провайдером номер мобильного
телефона/адрес электронной почты физического лица заблокирован и т.п.), МКК не
несет какой-либо ответственности.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Клиент самостоятельно несет
риски любых убытков или иных негативных последствий, которые могут возникнуть у
него в результате пользования услугами МП и выполнения им операций.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МКК не несет ответственность
за любые убытки, возникшие у Клиента, в том числе в связи с тем, что Клиент не
ознакомился и (или) несвоевременно ознакомился с условиями оферты и (или)
документами и (или) изменениями и дополнениями, внесенными в оферту и (или)
документы.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>МКК не несет ответственности,
если персональные данные, логин и пароль были преднамеренно/непреднамеренно
переданы/утеряны Клиентом и стали известны третьим лицам по вине Клиента.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Ущерб, причиненный Клиентом
вследствие неисполнения или ненадлежащего исполнения оферты, подлежит
безусловному возмещению Клиентом МКК.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>4.7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Стороны освобождаются от
ответственности за частичное или полное неисполнение обязательств по настоящей
Оферте, в случае возникновения обстоятельств непреодолимой силы (форс-мажора),
к которым относятся: стихийные бедствия, пожары, наводнения, военные действия,
вступление в силу законодательных актов, актов органов власти и управления,
обязательных для исполнения одной из сторон, прямо или косвенно запрещающих
указанные в настоящем соглашении виды деятельности, или вызванные иными
обстоятельствами вне разумного контроля Сторон, препятствующие выполнению
Сторонами своих обязательств по настоящей Оферте.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Разрешение споров</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>5.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>В случае возникновения споров,
возникающих из отношений между Клиентом и МКК, досудебный порядок
урегулирования спора является обязательным для Сторон. Стороны обязуются до
обращения в судебные органы направлять претензии (письменные предложения о
добровольном урегулировании спора) по реквизитам, указанным в МП и данных
предоставленных Клиентом при регистрации. Сторона. Получившая претензию,
обязуется в течение 15 дней предоставить мотивированный ответ на претензию.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>5.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>При не достижении сторонами
согласия споры, разногласия или требования, </span><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif'>возникающие из настоящей Оферты и договоров или в связи с ними,
рассматриваются в судебных органах в соответствии с законодательством
Кыргызской Республики, по месту нахождения МКК.</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;border:none'><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:.25in;text-align:justify;text-indent:-.25in;border:none'><b><span
lang=RU style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Прочие условия</span></b></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящий Договор считается
заключенным с даты акцепта Клиентом настоящей Оферты и действует до полного
исполнения обязательств по настоящему Договору.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Настоящая Оферта действует до
момента ее официального отзыва МКК. В случае официального отзыва оферты
информация об этом размещается на сайте МКК и в МП.</span></p>

<p class=MsoNormal style='margin:0in;text-align:justify;text-indent:0in;
border:none'><span lang=RU style='font-size:12.0pt;line-height:107%;font-family:
"Times New Roman",serif;color:black'>6.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=RU style='font-size:12.0pt;line-height:107%;
font-family:"Times New Roman",serif;color:black'>Во всем, что прямо не
предусмотрено настоящей офертой, стороны руководствуются законодательством
Кыргызской Республики.</span></p>

<p class=MsoNormal style='text-align:justify;border:none'><span lang=RU
style='font-size:12.0pt;line-height:107%;font-family:"Times New Roman",serif;
color:black'>&nbsp;</span></p>

</div>

</body>

</html>

`
