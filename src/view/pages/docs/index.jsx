import React from "react"
import { useEffect } from "react";

import Container from "../../containers/container";
import DocsContainer from "../../containers/docs-container";
import { oferta } from "./files/oferta";

import "./style.scss"


const Docs = () => {
    useEffect(()=>{
        window.scrollTo(0, 0)
    },[])
    return (
        <div className="docs-page">
            <Container>
                <div className="docs-page__block-title">
                    <h2 className="docs-page__title">Документы</h2>
                </div>
                <DocsContainer/>
            </Container>
        </div>
    )
}
export default Docs