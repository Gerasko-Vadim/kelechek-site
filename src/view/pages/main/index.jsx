import React from "react"
import Conditions from "../../components/conditions"

import DivideByThree from "../../components/divide-by-three"
import Header from "../../components/header"
import OpenApp from "../../components/open-app"
import StepInstallment from "../../components/step-installment"
import Recommendation from "../../components/recommendation"

import "./style.scss"
import ShopingCenter from "../../components/shopping-centers"
import Quetions from "../../components/questions"
import AboutApp from "../../components/about-app"
import YoutubeBlock from "../../components/youtube"
import RepaymentScheme from "../../components/repayment-scheme"
import Questionnaire from "../../components/questionnaire"
import { useEffect } from "react"
import { useDispatch } from "react-redux"


const Main = () => {

    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch({type:"SET_CLIENT"})
    },[])
    return (
        <div className="main">
            <OpenApp/>
            <Conditions/>
            <StepInstallment/>
            <DivideByThree/>
            <Recommendation/>
            <ShopingCenter/>
            <Quetions/>
            <AboutApp/>
            <YoutubeBlock/>
            <RepaymentScheme/>
            <Questionnaire/>
        </div>
    )
}
export default Main;