import React from "react"

import YouTube from 'react-youtube';
import Container from "../../containers/container";
import "./style.scss"

const VideoYoutube = () => {
    const videoId = "https://www.youtube.com/watch?v=v4dzXyqO1tY".split("v=")[1].split("&")[0]
    const opts = {
        height: '100%',
        width: '100%',

    };
    const _onReady = (e) => {
        console.log(e)
    }
    return (
        <YouTube className="youtube__video" videoId={videoId} opts={opts} onReady={_onReady} />
    )
}

const YoutubeBlock = () => {
    return (
        <div className="youtube">
            <Container>
                <div className="youtube__wrapper">
                    <div className="youtube__content">
                        <span>
                            ПОДРОБНЕЕ О ТОМ КАК<br />
                            ПОЛЬЗОВАТЬСЯ <br />
                            ПРИЛОЖЕНИЕМ И ЧТО В НЕМ <br />
                            МОЖНО ДЕЛАТЬ <br />
                        </span>
                    </div>
                    <div className="youtube__block-video">
                        <video
                            type="video/mp4"
                            style={{
                                width: "100%",
                                height: "100%"
                            }}
                            controls
                            currentTime={11.3}
                            src='./2.mp4' />
                    </div>
                </div>
            </Container>

        </div>
    )
}

export default YoutubeBlock;