import React from "react"
import Container from "../../containers/container";

import "./style.scss"

const Interest = () => {
    return (
        <div className="interest" id="interest">
            <Container>
                <div className="interest__block-title">
                    <span className="interest__title">
                        Мы помогаем Вашему бизнесу расти
                    </span>
                </div>

                <div className="interest__wrapper">
                    <div className="interest__card">
                        <span className="interest__card-title">45%</span>
                        <span className="interest__card-description">Увеличение среднего чека</span>
                    </div>
                    <div className="interest__card">
                        <span className="interest__card-title">2-2,5</span>
                        <span className="interest__card-description">раза увеличение ежедневной выручки</span>
                    </div>
                    <div className="interest__card">
                        <span className="interest__card-title">100 - 200 %</span>
                        <span className="interest__card-description">Привлечение новых клиентов</span>
                    </div>
                </div>
            </Container>
        </div>
    )
}
export default Interest;