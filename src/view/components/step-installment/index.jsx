import React from "react"

import "./style.scss"
import img1 from "../../../assets/img/step-installment/1.png"
import img2 from "../../../assets/img/step-installment/2.png"
import img3 from "../../../assets/img/step-installment/3.png"
import Container from "../../containers/container"

const StepInstallment = () => {
    return (
        <div className="step-installment" id="step-installment">
            <Container>
                <span className="step-installment__title">Как оформить рассрочку?</span>
                <div className="step-installment__wrapper">
                    <div className="step-installment__card">
                        <img alt="number" style={{width:"161px"}} src={img1} />
                        <span style={{left:"73px"}}>Скачать мобильное <br /> приложние cash2u</span>
                    </div>
                    <div className="step-installment__card">
                        <img alt="number" style={{width:"255px", height: "308px"}} src={img2} />
                        <span>Пройти идентификацию <br /> и заполнить анкету</span>
                    </div>
                    <div className="step-installment__card">
                        <img alt="number" style={{width:"255px", height: "308px"}} src={img3} />
                        <span>Дождаться ответа <br /> и получить деньги</span>
                    </div>
                </div>
            </Container>
        </div>

    )
}
export default StepInstallment;