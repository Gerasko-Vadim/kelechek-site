import React from "react"
import Slider from "react-slick"
import { AiOutlineArrowLeft, AiOutlineArrowRight } from 'react-icons/ai'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import next from "../../../../assets/img/slider/next.svg"
import prev from "../../../../assets/img/slider/prev.svg"
import "./style.scss"


const SampleNextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <AiOutlineArrowRight
      className={className}
      style={{
        ...style,
        height: "32px",
        width: "32px",
        color: "black"
      }}
      onClick={onClick} />
  )
}

const SamplePrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <AiOutlineArrowLeft
      className={className}
      style={{
        ...style,
        height: "32px",
        width: "32px",
        color: "black"
      }}
      onClick={onClick} />

  )
}


const SliderButton = () => {

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,

        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,

        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };

  return (
    <div className="slider">
      <Slider {...settings}>
        <button className="slide-btn" >Популярные магазины</button>
        <button className="slide-btn">Одежда и обувь</button>
        <button className="slide-btn">Для дома</button>
        <button className="slide-btn">Детские товары</button>
        <button className="slide-btn" >Популярные магазины</button>
        <button className="slide-btn">Одежда и обувь</button>
        <button className="slide-btn">Для дома</button>
        <button className="slide-btn">Детские товары</button>
      </Slider>
    </div>
  )
}
export default SliderButton;