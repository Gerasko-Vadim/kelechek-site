import React from "react"
import Container from "../../containers/container"

import Galery from "./galery"
import SliderButton from "./slider-button"

import "./style.scss"

const Recommendation = () => {
    return (
        <div className="recommendation">
            <Container>
                <div className="recommendation__block-title">
                    <span className="recommendation__title">Покупайте с cash2u </span>
                    <span className="recommendation__subtitle">ваши любимые магазины только c нами</span>
                </div>
                <SliderButton/>
                <Galery/>
            </Container>

        </div>
    )
}
export default Recommendation;