import React from "react"
import emailjs from 'emailjs-com';

import { Button, Form } from "react-bootstrap";
import Container from "../../containers/container";

import "./style.scss"
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const FeedbackForm = () => {

    const onSubmit = (e) => {
        e.preventDefault()
        console.log(e.target)
        emailjs.sendForm('service_77fxlhd', 'template_rzr0psk', e.target, 'user_kVb54jMRjyVMOolEvXPco')
            .then((result) => {
                toast.success("Заявка отправлена успешно !!")
            }, (error) => {
                toast.error("Сообщение не отправлено :(")
            });
        e.target.reset()

    }

    return (
        <div className="feedback-form" id="feedback-form">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <Container>
                <div className="feedback-form__wrapper">
                    <span className="feedback-form__title">Заполните заявку и станьте партнером!</span>
                    <span className="feedback-form__description">Общая информация</span>
                    <Form style={{ width: "100%" }} onSubmit={onSubmit}>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="city" size="lg" type="text" placeholder="Город и район" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="contact_name" size="lg" type="text" placeholder="Контактное имя" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="phone_number" size="lg" type="phone" placeholder="Мобильный телефон" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="email" size="lg" type="email" placeholder="Электронная почта" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="position" size="lg" type="text" placeholder="Должность" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="name_of_compony" size="lg" type="text" placeholder="Наименование организации" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="adress" size="lg" type="text" placeholder="Адрес" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="category" size="lg" type="text" placeholder="Категории товаров и услуг" />
                        </Form.Group>
                        <Form.Group style={{ textAlign: "left" }} className="mb-4" >
                            <span className="feedback-form__description">Информация по магазинам</span>
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="name_shop" size="lg" type="text" placeholder="Название магазина (Бренда)" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="web_adress" size="lg" type="text" placeholder="Веб адрес интернет магазина" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="number_shop" size="lg" type="text" placeholder="Количество магазинов" />
                        </Form.Group>
                        <Form.Group className="mb-4 input" >
                            <Form.Control className="feedback-form__inputs" required={true} name="total_sum_to_month" size="lg" type="text" placeholder="Общая сумма продаж в месяц" />
                        </Form.Group>
                        <div className="questionnaire__form-checkbox-block">
                            <input type="checkbox" id="check1" required className="checkbox" />
                            <label htmlFor="check1" className="check-text">
                            </label>
                            <span> Выражаю согласие на обработку персональных данных и подтверждаю,что ознакомлен
                                с <b>Политикой</b> обработки персональных данных</span>
                        </div>
                        <div className="feedback-form__blockBtn">
                            <Button className="btn-submit" type="submit" >
                                Отправить
                            </Button>
                        </div>
                    </Form>
                </div>
            </Container>

        </div>
    )
}
export default FeedbackForm;