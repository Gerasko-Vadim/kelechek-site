import React from "react"
import "./style.scss"
import classNames  from "classnames"

const ConditionItem = ({img, text, className}) => {
    return (
        <div className={classNames("condition-item", className)} >
            <img src={img} />
            <div className="condition-item__content">
                <span>
                    {text}
                </span>
            </div>
        </div>
    )
}
export default ConditionItem;