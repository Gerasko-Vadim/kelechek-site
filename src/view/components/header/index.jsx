import React, { useState } from "react"
import "./style.scss"
import logo from "../../../assets/img/logo-for-header.svg"
import classNames from "classnames"
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { Link } from "react-router-dom"
import { useSelector } from "react-redux"
import ClientHeader from "./clientHeader"
import PartnerHeader from "./partnerHeader"


const Header = () => {
    const [language, setLanguage] = useState(1)
    const isClient = useSelector((state) => state.HandleChanheHeader.isClient)
    return (
        <div className="header">

            {
                isClient ? <ClientHeader language={language} setLanguage={setLanguage}/> : <PartnerHeader language={language} setLanguage={setLanguage}/>
            }

        </div>
    )
}
export default Header;

