import React from "react"
import logo from "../../../../assets/img/logo-for-header.svg"
import classNames from "classnames"
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { Link } from "react-router-dom"

const PartnerHeader = ({
    setLanguage,
    language
}) => (
    <Navbar expand="xxl"
    >
        <Navbar.Brand><Link to="/"><img src={logo} className="header__logo" /></Link></Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav" className="nav-content">
            <Nav className="me-auto">
                <Nav.Link href="#brend-kg">Бренды</Nav.Link>
                <Nav.Link href="#interest">Помогаем бизнесу</Nav.Link>
                <Nav.Link href="#benefit">Бизнесу выгодно</Nav.Link>
                <Nav.Link href="#how-it-works">Как это работает</Nav.Link>
                <Nav.Link href="#feedback-form">Стать партнером</Nav.Link>
            </Nav>
            <Nav>
                <Nav className="header__switch-lang">
                    <a className={classNames('header__lang', {
                        'header__lang-active': language === 1
                    })} onClick={() => setLanguage(1)}>RU</a>
                    <a className={classNames('header__lang', {
                        'header__lang-active': language === 2
                    })} onClick={() => setLanguage(2)}>KG</a>
                </Nav>
                <Nav.Link eventKey={2} target="_blank" href="https://frosty-curie-205c4a.netlify.app/">
                    <button className="header__get-credit">
                        Личный кабинет
                    </button>
                </Nav.Link>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
)

export default PartnerHeader;