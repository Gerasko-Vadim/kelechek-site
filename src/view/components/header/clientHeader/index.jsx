import React from "react"
import logo from "../../../../assets/img/logo-for-header.svg"
import classNames from "classnames"
import {  Nav, Navbar } from "react-bootstrap"
import { Link } from "react-router-dom"

const ClientHeader = ({
    setLanguage,
    language
}) => (
    <Navbar expand="xxl"
    >
        <Navbar.Brand><Link to="/"><img src={logo} className="header__logo" /></Link></Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav" className="nav-content">
            <Nav className="me-auto">
                <Nav.Link href="#conditions">Условия</Nav.Link>
                <Nav.Link href="#shoping-center">Покупки</Nav.Link>
                <Nav.Link href="#step-installment">Как оформить?</Nav.Link>
                <Nav.Link href="#about-app">О Приложении</Nav.Link>
                <Nav.Link href="#repayment-schema">Как погасить?</Nav.Link>
                <Link className="nav-link" to="/business">Для бизнеса</Link>
            </Nav>
            <Nav>
                <Nav className="header__switch-lang">
                    <a className={classNames('header__lang', {
                        'header__lang-active': language === 1
                    })} onClick={() => setLanguage(1)}>RU</a>
                    <a className={classNames('header__lang', {
                        'header__lang-active': language === 2
                    })} onClick={() => setLanguage(2)}>KG</a>
                </Nav>
                <Nav.Link eventKey={2} href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                    <button className="header__get-credit">
                        Получить кредит
                    </button>
                </Nav.Link>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
)

export default ClientHeader;