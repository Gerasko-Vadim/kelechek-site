import React from "react"
import Container from "../../containers/container"
import phone from "../../../assets/img/how-it-works/phone.png"
import pc from "../../../assets/img/how-it-works/pc.png"
import money from "../../../assets/img/how-it-works/money.png"
import clock from "../../../assets/img/how-it-works/clock.png"
import "./style.scss"

const HowItWorks = () => {
    return (
        <div className="how-it-works" id="how-it-works">
            <Container>
                <div className="how-it-works__block-title">
                    <span className="how-it-works__title">
                        Как это работает:
                    </span>
                </div>
                <div className="how-it-works__wrapper">
                    <div className="how-it-works__block-column-item">
                        <img alt="pc" src={pc} className="icon" />
                        <div className="how-it-works__content">
                            <span>
                                1. Клиент выбирает <br /> товар  или услугу <br /> в магазине Партнера
                            </span>
                        </div>
                    </div>
                    <div className="how-it-works__block-column-item">

                        <div className="how-it-works__content">
                            <span>
                                2. Оплачивает покупку <br />
                                в рассрочку на кассе <br />
                                QR кодом <br />
                                в приложении cash2u<br />
                            </span>
                        </div>
                        <img alt="phone" src={phone} className="icon" />

                    </div>
                    <div className="how-it-works__block-column-item">
                        <img alt="money" src={money} className="icon" />
                        <div className="how-it-works__content">
                            <span>
                                3. Еженедельное <br />
                                зачисление денег <br />
                                за проданный товар <br />
                                на счет Партнера
                            </span>
                        </div>
                    </div>
                    <div className="how-it-works__block-column-item">
                        <div className="how-it-works__content">
                            <span>
                                4. Клиент возвращает <br />
                                сумму покупки<br />
                                компании cash2u<br />
                                равными частями<br />
                                в течении 3 месяцев
                            </span>
                        </div>
                        <img alt="clock" src={clock} className="icon" />

                    </div>
                </div>
            </Container>

        </div>
    )
}

export default HowItWorks;