import React from "react"

import "./style.scss"
import cash from "../../../assets/img/conditions/vaadin_cash.svg"
import calendar from "../../../assets/img/conditions/topcoat_calendar.svg"
import file from "../../../assets/img/conditions/mdi_file-percent-outline.svg"
import slesh from "../../../assets/img/conditions/fa-solid_handshake-slash.svg"
import clock from "../../../assets/img/conditions/Group.svg"
import mobilephone from "../../../assets/img/conditions/emojione-monotone_mobile-phone-with-arrow.svg"
import ConditionItem from "../condition-item"
import 'animate.css/animate.css'
import RubberBand from 'react-reveal/RubberBand';
import Fade from 'react-reveal/Fade';
import Container from "../../containers/container"


const arrCards = [
    { id: 0, img: cash, text: 'Рассрочка на 5000, 10000 и 15000 сом' },
    { id: 1, img: calendar, text: 'Финансирование до 24 месяцев' },
    { id: 2, img: file, text: 'Рассмотрение и выдача в течении 15 минут' },
    { id: 3, img: slesh, text: 'Без переплат и процентов' },
    { id: 4, img: clock, text: 'Получение рассрочки онлайн - не выходя из  дома' },
    { id: 5, img: mobilephone, text: 'Без поручительства' },
]

const Conditions = () => {
    return (
        <div className="conditions" id="conditions">

            <span className="conditions__title ">
                Условия
            </span>


            <Container>
                <div className="conditions__wrapper">
                    {
                        arrCards.map((item) => {
                            return (
                                <ConditionItem className="" key={item.id} img={item.img} text={item.text} />

                            )


                        })
                    }
                </div>
            </Container>

        </div >
    )
}
export default Conditions;