import React from "react"
import "./style.scss"
import { FileTextOutlined } from '@ant-design/icons'
import { Link } from "react-router-dom"

export const DocItem = ({ name, pathname }) => (

    <div className="doc-item">
        <FileTextOutlined className="doc-icon" />
        <Link to={`/doc/${pathname}`}>
            <div style={{ overflowX: "hidden", wordWrap: "break-word" }}>
                <span className="doc-item__name-file">{name}</span>
            </div>
        </Link>
    </div>
)