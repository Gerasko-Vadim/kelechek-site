import React from "react"

import "./style.scss"
import qr from "../../../assets/img/become-partner/qr-code.png"
import Container from "../../containers/container"

const Benefit = () => {
    return (
        <div className="benefit" id="benefit">
            <Container>
                <div className="benefit__block-title">
                    <span className="benefit__title">
                        Бизнесу выгодно с cash2u
                    </span>
                </div>
                <div className="benefit__wrapper">
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            Маркетинговые инструменты для привлечения новых клиентов
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            Зачисление выручки на р/счет, кошелек, карту Партнера
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            Получение конкурентного преимущества
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            Простота и удобность приема оплаты за товары и услуги
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            МКК берет на себя все риски по возврату денег от клиента
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            Расширение географии ваших продаж
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            Доступ к личному кабинету  Партнера
                        </span>
                    </div>
                    <div className="benefit__card">
                        <img alt="qr" src={qr} />
                        <span className="benefit__card-description">
                            Долгосрочное сотрудничество и доступ к новой базе клиентов
                        </span>
                    </div>
                </div>

            </Container>
        </div>
    )
}

export default Benefit;