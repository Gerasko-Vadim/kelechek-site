import React from "react"
import Container from "../../containers/container";

import "./style.scss"

const RepaymentScheme = () => {
    return (
        <div className="repayment-schema" id="repayment-schema">
            <Container>
                <span className="repayment-schema__title">Как погасить?</span>
                <div className="repayment-schema__cards-wrapper">
                    <div className="repayment-schema__card">
                        <div className="repayment-schema__black-card"></div>
                        <span className="repayment-schema__card-title">Платежные <br /> терминалы</span>
                        <button className="repayment-schema__card-btn">
                            <span>АДРЕСА ТЕРМИНАЛОВ</span>
                            <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0354374 7.81015L13.4363 7.74886L8.59484 12.4716L9.89969 13.7213L16.9492 6.84472L9.83709 0.0328569L8.54372 1.29451L13.4281 5.97275L0.0273145 6.03404L0.0354374 7.81015Z" fill="#525252" fill-opacity="0.96" />
                            </svg>
                        </button>
                        <div className="repayment-schema__card-capabilities">
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>Внесение без комиссии</span>
                            </div>
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>Моментальное зачисление</span>
                            </div>
                        </div>
                    </div>
                    <div className="repayment-schema__card">
                        <div className="repayment-schema__black-card"></div>
                        <span className="repayment-schema__card-title">Электронные<br/>кошельки</span>
                        <button className="repayment-schema__card-btn">
                            <span>ПЕРЕЙТИ К ОПЛАТЕ</span>
                            <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0354374 7.81015L13.4363 7.74886L8.59484 12.4716L9.89969 13.7213L16.9492 6.84472L9.83709 0.0328569L8.54372 1.29451L13.4281 5.97275L0.0273145 6.03404L0.0354374 7.81015Z" fill="#525252" fill-opacity="0.96" />
                            </svg>
                        </button>
                        <div className="repayment-schema__card-capabilities">
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>Внесение без комиссии</span>
                            </div>
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>Моментальное зачисление</span>
                            </div>
                        </div>
                    </div>
                    <div className="repayment-schema__card">
                        <div className="repayment-schema__black-card"></div>
                        <span className="repayment-schema__card-title">Оплата через <br/>Айылбанк</span>
                        <button className="repayment-schema__card-btn">
                            <span>АДРЕСА ФИЛЛИАЛОВ</span>
                            <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0354374 7.81015L13.4363 7.74886L8.59484 12.4716L9.89969 13.7213L16.9492 6.84472L9.83709 0.0328569L8.54372 1.29451L13.4281 5.97275L0.0273145 6.03404L0.0354374 7.81015Z" fill="#525252" fill-opacity="0.96" />
                            </svg>
                        </button>
                        <div className="repayment-schema__card-capabilities">
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>Внесение без комиссии</span>
                            </div>
                            <div className="repayment-schema__card-capabilities-item">
                                <svg width="15" height="12" viewBox="0 0 15 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.707 8.58597L1.414 5.29297L0 6.70697L4.707 11.414L14.414 1.70697L13 0.292969L4.707 8.58597Z" fill="#BDB8B8" />
                                </svg>
                                <span>Моментальное зачисление</span>
                            </div>
                        </div>
                    </div>

                </div>
            </Container>
        </div>
    )
}

export default RepaymentScheme;