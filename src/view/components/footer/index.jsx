import React from "react"
import "./style.scss"
import ok from "../../../assets/img/footer/ok.svg"
import facebook from "../../../assets/img/footer/facebook.svg"
import instagram from "../../../assets/img/footer/instagram.svg"
import telegram from "../../../assets/img/footer/telegram.svg"
import vk from "../../../assets/img/footer/vk.svg"
import youtube from "../../../assets/img/footer/youtube.svg"
import playMarket from "../../../assets/img/footer/playMarket.png"
import { Link } from "react-router-dom"
import { useSelector } from 'react-redux'

const Footer = () => {

    const isClient = useSelector((state) => state.HandleChanheHeader.isClient)
    return (
        <div className="footer">
            <div className="footer__block-inforvation">
                <div className="footer__wrapper-block-links">
                    <div className="footer__links">
                        {
                            isClient ? <>
                                <span className="footer__links-title">Общая информация</span>
                                <a className="footer__link" href="#about-app">О приложении </a>
                                <a className="footer__link" href="#divide-by-three">Способы оплаты кредита</a>
                                <Link to="/docs" className="footer__link"><span >Документы</span></Link>
                            </>
                                :
                                <>
                                    <span className="footer__links-title">Общая информация</span>
                                    <a className="footer__link" href="#how-it-works" >Как это работает </a>
                                </>
                        }

                    </div>
                    <div className="footer__links">
                        <span className="footer__links-title">cash2u</span>
                        {
                            isClient ? <>
                                <a className="footer__link" href="#questions">Вопросы и ответы</a>
                                <a className="footer__link" href="#questionnaire">Акции</a>
                            </>
                                :
                                <>
                                    <a className="footer__link" href="#benefit">Бизнесу выгодно</a>
                                    <a className="footer__link" href="#feedback-form">Стать партнером</a>
                                </>
                        }

                    </div>
                </div>
                <div className="footer__social">
                    <div className="footer__block-phone">
                        <span className="footer__phone">0772 55 33 33</span>
                        <span className="footer__phone">0552 55 33 33</span>
                        <span className="footer__phone">0502 55 33 33</span>
                        <span className="footer__block-phone-descrip">Для звонков по Кыргызстану</span>
                    </div>
                    <div className="footer__social-icons">
                        <a target="_blank" href="https://m.ok.ru/dk?st.cmd=userProfile&tkn=7934&_prevCmd=userSettingsPersonal&__dp=y&_aid=leftMenuClick">
                            <img src={ok} alt="icon" />
                        </a>

                        {/* <img src={telegram} alt="icon" /> */}
                        <a target="_blank" href="https://www.instagram.com/cash2u.kg/">
                            <img src={instagram} alt="icon" />
                        </a>
                        <a target="_blank" href="https://youtube.com/channel/UCYd4ljGJODrg1mCLNLl85aA">
                            <img src={youtube} alt="icon" />
                        </a>
                        <a target="_blank" href="https://vk.com/cash2u">
                            <img src={vk} alt="icon" />
                        </a>
                        <a target="_blank" href="https://www.facebook.com/Cash2u-102189698862906/">
                            <img src={facebook} alt="icon" />
                        </a>

                    </div>
                    {
                        isClient
                            ? <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                                <img alt="play-market" className="footer__playMarket" src={playMarket} />
                            </a>
                            : <a href="https://play.google.com/store/apps/details?id=kg.cash2u.business" target="_blank">
                                <img alt="play-market" className="footer__playMarket" src={playMarket} />
                            </a>
                    }

                    <div className="footer__block-address">
                        <span>
                            «ОсОО «МКК «Келечек». Запись об учетной регистрации №510 от 06.08.2021 г.»
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Footer;