
import React from "react"

import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.scss"
import { Form, Button } from "react-bootstrap";
import Container from "../../containers/container";


const Questionnaire = () => {
    return (
        <div className="questionnaire" id="questionnaire">
            <Container>
                <div className="questionnaire__wrapper">
                    <span className="questionnaire__title">Заполнив анкету вы сможете:</span>
                    <span className="questionnaire__description">
                    Получать уведомления о специальных акциях и бонусах только
                    для пользователей приложения «cash2u». Поэтому если Вам интересно
                        скорее заполняй анкету и получай доступ к лучшим акция и бонусам
                    </span>
                    <div className="questionnaire__form">
                        <span className="questionnaire__form-title">Узнать о акциях и бонусах сейчас!</span>
                        <Form style={{width:"100%"}}>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Control style={{
                                    border: "1px solid #4F4F4F",
                                    background: "#FFF9F9"
                                }}  size="lg" type="email" placeholder="Электронная почта" />
                            </Form.Group>
                            <div className="questionnaire__form-checkbox-block">
                                <input type="checkbox" id="check1" required={true} className="checkbox" />
                                <label htmlFor="check1" className="check-text">
                                   
                                </label>
                                <span> Выражаю согласие на обработку персональных данных и подтверждаю,что ознакомлен
                                    с <b>Политикой</b> обработки персональных данных</span>
                            </div>


                            <Button className="btn-submit" type="submit" >
                                Отправить
                            </Button>
                        </Form>

                    </div>
                </div>
            </Container>
        </div>
    )
}

export default Questionnaire;