import React from "react"
import pc from "../../../assets/img/about-app/pc.png"
import phone from "../../../assets/img/about-app/phone.png"
import qrCode from "../../../assets/img/about-app/qr-code.svg"
import "./style.scss"

const AboutApp = () => {
    return (
        <div className="about-app" id="about-app">
            <div className="about-app__block-img">
                <div className="about-app__image-elips" />
                <img src={pc} className="about-app__pc" />
                <img src={phone} className="about-app__phone" />
            </div>
            <div className="about-app__block-content">
                <span className="about-app__title">О ПРИЛОЖЕНИИ</span>
                <span className="about-app__description">
                    Мобильный офис который  всегда с вами <br />
                    - все сферы услуг для рассрочки онлайн  24/7<br />
                    - оплата телефоном без лишних деталей<br />
                    - различные акции и бонусы от партнеров
                </span>
                <div className="about-app__block-activity">
                    <img src={qrCode} />
                    <span></span>
                    <a href="https://play.google.com/store/apps/details?id=kg.cash2u.client" target="_blank">
                        <button>Скачать приложение</button>
                    </a>
                </div>
            </div>
        </div>
    )
}
export default AboutApp;