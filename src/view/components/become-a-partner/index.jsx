import React from "react"

import "./style.scss"
import phones from "../../../assets/img/become-partner/phones.png"
import Container from "../../containers/container"

const BecomePartner = () => {
    return (
        <div className="become-a-partner">
            <Container>
                <div className="become-a-partner__wrapper">
                    <div className="become-a-partner__content">
                        <span className="become-a-partner__title">
                            Универсальное<br />приложение для<br />приема оплаты
                        </span>
                        <span className="become-a-partner__description">Простое и удобное приложение для приема оплаты за товары и услуги</span>
                        <a href="https://play.google.com/store/apps/details?id=kg.cash2u.business" target="_blank" className="become-a-partner__btn">
                            Скачать приложение
                        </a>
                    </div>
                    <div className="become-a-partner__blockImg">
                        <div className="become-a-partner__elips" />
                        <img alt="phone" src={phones} />
                    </div>
                </div>
            </Container>

        </div>
    )
}
export default BecomePartner;