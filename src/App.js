import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Main from './view/pages/main';
import Footer from "../src/view/components/footer"
import Header from './view/components/header';
import Business from './view/pages/business';
import Docs from './view/pages/docs';
import Doc from './view/pages/doc';
import PrivacyPolicy from './view/pages/privacyPolicy';

function App() {
  return (
    <div className="App">

      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Main} />
          <Route path="/business" component={Business} />
          <Route path="/docs" component={Docs} />
          <Route path="/doc/:name" component={Doc} />
          <Route path="/privacy-policy" component={PrivacyPolicy} />
        </Switch>
        <Footer />
      </Router>

    </div>
  );
}

export default App;
